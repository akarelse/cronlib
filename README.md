# Calendar for PyroCMS

## Legal

 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 * 
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

## Overview

A library for getting due dates from datetime and a cron string.

---

## Features

next time the cron string will trigger in unix time stamp
the previous time the cron string did trugger in unix time stamp

---

## Widgets

None

---

## Settings

None

---

## Installation

Git clone it.

---

## Credits

Jan Konieczny <jkonieczny@gmail.com> for making the original lib.


---

## ToDo

+ a bit more testing
+ perhaps a extra function here or there, if you need it contact me.
+ an other way of interaction than unix time
+ the now timestamp as class variable

